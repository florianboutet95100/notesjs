                                    **Les Spread operator**      
_les spread  se sont les 3 petits ...  cela veut dire étaler._            
    
**regardons l'exemple du spread ...**       
    
let arr1 = ["Jean", "Sam", "Tom"];  
let arr2 = ['Pauline','Lise','Eloise']; 

let arr3 = [];  

for(let 1 =0; i < arr1.length; i++) {   
    arr3.push(arr1[i]); 
    
}   
console.log(arr3);              
    
**_Avec le spread opérator je peu mettre une ligne en remplaçant les 6 lignes  
exemple:_**    

let arr1 = ["Jean", "Sam", "Tom"];  
let arr2 = ['Pauline','Lise','Eloise'];     
    
let arr3 = [...arr1, ...arr2];          

console.log(arr3, ....arr2);        
        
    
**_Elles peuvent aussi être utilisées dans des objets._       
Exemple:**    

let studentDefault = {  
    name: "Jean", 
    class; null,

};  
let student = { ...studentDefault, presence: true};     
    
let student2 = {...student};
      
 student2.name = 'lise'; 

};          
console.log(student.name,student2.name);

        
    
**_Comme pour les tableaux_**       
    
let arr1 = [1, 3];  
let arr1 = [1, 3];  _**(==> la référence pointe la donnée_**    
arr2.push(5);       

console.log(arr1, arr2);            


let arr1 = [1, 3];  
let arr1 = [...arr1];   **_application du spread ..._** 

arr2.push(5);       
console.log(arr1, arr2);                
    
    
    
 


    
