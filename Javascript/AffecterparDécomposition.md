             **_Affecter par décomposition_**          
_par exemple sur un tableau:_

let a,b;    
[a, b =20] = [1,40];    
console.log(a);     
console.log(b);

_exemple pour la partie objet:_         
let name, age;  
let student = {name: 'jean', class: "B", age: 20}:
({name, age} = student);        
console.log('name:', name);