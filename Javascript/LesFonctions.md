            **_Javascript - Les fonctions : Déclarer une fonction_**        
    
                            _Les fonctions_     

_une fonction est un ensemble d'instruction éffectuant une tache ou calculant une valeur 
    une fonction peut retourner des données ou non_      

_Pour définir une fonction il faut utilisé le mots clef_  **_function**_    

 _et on nomme la fonction par exemple qui multiplie deux données entre elle_          
    
Après avoir nommée la fonction on ouvre la parenthése et on indique les arguments de la fonction.   
au niveau des isntructions on fait le code, la tâche qu'on souhaite.        
    
_dans le cas je retourne une donnée le mots clef sera_ **_return_** dans tout type de données

exemple
    
function multiplyby(nb, nb2) {  
    //instructions      
console.log('nb :', nb);        
console.log('nb2 :',nb2);       
    
return nb * nb 2; _cette fonction me permet de multiplier des éléments entre eux._

}       
let produit = multiplyby(nb42, nb2:60);

_-si j'invoque la fonction_     
les () je l'invoque et je dois donner les paramétres de la fonction     

console.log('produit' :, produit);          

une fonction elle peut faire une tache que je souhaite. 
    
function checkValue(value)  {       
console.log(value === 30);      

}   
chekValue(value:30);    dans la console il indiquera true 

chekValue(value:33);  dans la console il indiquera false            

D'autres arguments par exemple qui ont des valeurs par défauts      
exemple             

function checkValue(value, value3:number = 2)  { 
console.log(value >= value3, value >= value3);          
return `ok`;    

_/value 3 équivaut 2 par défaut /_

}           
chekValue(value:30);            
chekValue(value:33, value3:40);     
chekValue(value:5, value3:4);          
chekValue(value:2);                 

            
_autre exemple comme déclaré un tableau avec des valeurs._      
    
let arr = [2, 3, 4, 9, 349, 49, 49];            
    
function afficheChaqueValeur(arr) {         
console.log('Tableau :', arr);
    
for (let i= 0; i<arr.lenght; i++)   {       
    
console.log(arr[i]);

  }

}       
    
afficheChaqueValeur(arr);           
    
    
    
    
    

                    Javascript - Les fonctions - Fonction anonyme
**LES FONCTIONS**       
    
_Est une fonction qui ne porte pas de nom_  
    
let fn = function () {      
    retourn 1;

};      
console.log(fn)), fn ());               


                    **_Javascript - Les fonctions : Fonction callback_**        

    
est une fonction qui va passer en argument et qui va être invoqué par une autre fonction        

function maCallback(nb) {       
    return nb * 3;

}       
function maFonction(nb, callback) {     
    let var1 = nb * 16;     
    return callback(var1);

}       
let res = monFonction(nb6, maCallback);       
console.log(res);




                    **_Javascript - Les fonctions : l'objet arguments_**            
    
_on délacare une fonction maFn qui prends un argument une multidude d'arguments._           
exemple:
    

function maFn() {       
if(arguments[5] ==== "Sam")         
alert('Vous êtes Sam');

console.log(arguments[0],arguments[5]);

}       
mafn(1, 2, 3 ,4 ,5 ,6, "jean", true, null); <=) invocation      
mafn(1, 2, 3 ,4 ,5 ,6, "Sam", true, null);




                        **_Javascript - Les objets : Déclarer un objet_**       
_Un objet est la représentation de clef valeur.  

Chaques caractéristiques lié a un objet sont des propriétés.      
toutes les actions liés a une action sont des méthodes.     

Comment déclarer un objet:          
     Un objet c'est la réprésentation  de la donnée sous forme de clef valeur_
    
//  { key: value, key: value }

let phone = {       
    
//Propriété
    
name: 'Sony',       
marque: 'Xpéria',   
largeur: 5, 
longueur: 5,        
    
    
//Méthodes      
_Pour définir une méthode sera toujours clef valeur._           
call: function ()   {       
    console.log('Call Sam');

},      
sendMsg: function (to)  {       
  console.log('send sms to' + to);  <=) envoyé sms to a to qui est Sam      
    
Pour accéder a des propriétés:      
console.log('Marque:',phone.marque);        
phone.call();       
phone.sendMsg(to'Sam');

    
}
    

};




                    **_Javascript - Les objets : Accéder à l’objet courant dans une méthode_**          
// {key: value, key: value }            
    
let student = {     

name:'Sam'  
class:'Terminal',   
say: function (say) {   
    // "NAME : BONJOUR"  
console.log(this.name);

}
    

};      
student.say(say:'Bonjour');

    
    


    
   
      
