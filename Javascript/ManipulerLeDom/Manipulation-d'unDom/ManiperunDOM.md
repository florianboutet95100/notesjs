**Manipuler le dom : S'assurer que le dom est chargé**      
    
_Mon navigateur  lorsque qu'il récupére une page html il va éxécuter chaques      
lignes les unes après les autres qui va interpréter d'abord dans la balise 'Head'   
qui est le titre de la page._    

les métadonnées la description des mots clefs lié à la page et ensuite interpréter les structures de la page qui sont      
la balise textuelle  h1, la balise de paragraphe ect.... interpréter après la balise Head   


dans l'ordre on affiche le titre à l'utilisateur et ensuite les éléments JavaScript se lient à la fin de la balise body     
pour le mettre en dernier pour charger le Javascript.       
        
_pour lié un fichier js je rajoute le script et la source 'src'_      
    
exemple **<script src="index.js"></script>** => a voir dans le fichier index.js .             
    
et pour accéder a ce fichier pour lié à la page html je lui indique le chemin d'accée qui se trouve     
dans le dossier app.js avec l'attribue src.     
     
    
            
        
_Comment s'assurer que le dom est charger?       
     
Le dom se sont des éléments html qui seront afficher à l'utulisateur._   

Dans la console index.js je sélectionne mon titre       
**console.log(document.querySelector('h1'));**      
            
_Par exemple si le script srs est présent dans le_ **head** on utilise une méthode pour voir que le dom est charger    
dans la console index.js pour voir les proprités et manupuler la fenêtre grace à l'objet window
console.log(window);    
    
on peu rajouter à l'objet window la propriété onLoad et lui donner une fonction,     

window.onLoad = function cette propriété va être excuter à la fin du téléchargement du dom      
    
si mon querySelector je l'applique dans ma fonction elle affiche après le dans la console et je récupére mon titre.     

/\ A VOIR DANS LA CONSOLE INDEX.JS sur l'explication /\
        
Une autre méthode par exemple dans l'objet window  dans la console.     

window.addEventListener('DOMContentLoaded',function(){      

console.log('5');   

console.log(document.querySelector('h1'));

});     
            
seulement à la fin d du téléchargement du dom il execute la fonction qui affiche 5 et récupére le titre.        


Il faudra toujours entouré le script qui manipule le dom par    

window.onload = function() {
console.log('4')
console.log(document.querySelector('h1'));
};          
            
ou alors par        

window.addEventListener('DOMContentLoaded',function(){
console.log('5');
console.log(document.querySelector('h1'));

});



**Manipuler le dom : Sélectionner un élément grâce à son id**       


    
