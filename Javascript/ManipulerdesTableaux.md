
                        **Manipuler les tableaux**  
**_les méthodes pour manipuler les tableaux._**
source: https://www.youtube.com/watch?v=bw5S9DJTM9k&feature=youtu.be

_voyons les méthodes;_    
.forEach() permet d'effectuer une fonction sur chaque élément d'un tableau

.find() / .findIndex  permet trouver un élèment dans un tableau en fonction d'un tableau.   
fonction d'une condition si c'est fonction  retourne d'un boolean true on va pouvoir le Save

.findIndex() il va retourner l'index de l'élément qui renvois true

.map() permet de modifier et de crée un tableau dans un tableau.

.reduce() permet de reduire un tableau a une certaine valeur ou une autre forme

.filter() permet de filtré les éléments d'un tableau

.some() permet de retourné true si une des valeurs coorespond a une condition.

.every() permet de retourné vrai si toutes les valeurs respecte une condition. 
    

    


                                                        **La méthode "forEach"**       
_comment manipuler un tableau avec la méthode "forEach"_        
    
let arr = [1, 4, 6, 8];        
arr.forEach(function (value:number, index:number)) {     
console.log(index,value);
  
});             


                                                         ** La méthode " find"**     
    
_permet de récupérer une valeur en fonction d'une condition_        

let arr = ["jean", "Sam", "Steve", "Tom"];  
let callback = function (name, index) {       
if(name.include(searchElement:'o')) {    
return true;    
} else {        
  return fasle; 
    
}

};  
let res = arr.fin(callback);

**_Cette opération peut être simplifié.**_      

let arr = ["jean", "Sam", "Steve", "Tom"];  
let callback = function (name, index) {         
return name.include(searchElement: 'o');    
};      
let res = arr.find(callback);       
console.log(' res :', res);             




                **La méthode "findIndex"**          
_permet de retourner l'index d'un élément qui respect une condition_            

let arr = ["jean", "Sam", "Steve", "Tom"];      
let callback = function (element, index) {      
console.log(element, index);        
return element.length === 5;

    
};  
let index arr.findIndex(callback);      
console.log('index:', index, arr[index]);       



                        **La méthode "map"**    
_Cette méthode permet de crée un tableau a partir d'un autre tableau._    


let arr [1, 2, 4, 56, 78, 99, 123, 456];      
let callback = function (value, index) {    
  return value * 2;

};    
let arr2 = arr.map(callback);   
console.log(arr2);
    
  
autre exemple: avec des objets:       
let arr = [     
{age: 22},  
{age: 17},    
{age: 56},    
];    
      
let callback = function (value, index) {    
console.log(value);   
return value.age;

};    
let arr2 = arr.map(callback);   
console.log(arr2);




                            **La méthode "reduce"**   
_la méthode reduce permet reduire un tableau sous une donnée ou un autre type de données._    
je souhaite réduire mon tableau en un nombre .    
  
let arr = [3, 4, 49 , 230, 48, 123];   
  
let accumulation = arr.reduce(functiona(acc:number,value:number, index:number);{     
console.log(acc,value,index);   
acc = acc + value;  
return acc;

}, 0);    
console.log(accumulation, arr);  

**acc veut dire acumulateur.**        
  
_on peut aussi crée un nouveau tableau_     

let arr = [3, 4, 49 , 230, 48, 123];

let accumulation = arr.reduce(functiona(acc:number,value:number, index:number);{     
console.log(acc,value,index);   
acc.push(value); 
return acc;

}, [];    
console.log(accumulation, arr);           


                              **La méthode "filter"**         
_Cette méthode permet de filtrer des éléments d'un tableau en fonction d'une condition._        

let arr = [3, 4, 49 , 230, 48, 123];    
let arrfiltred = arr.filter(function(element : number, index : number) {  
console.log(element, index);  
return true;  

});     
console.log('arry flitré :', arrfiltred);       



                          **La méthode "some"**     
      
  **_Cette méthode elle est utilisé si au moins une valeur du tableau si elle (true) valide la condition  
elle retournera false si aucune des valeurs valide la condition**_      

let arr = [3, 4, 49 , 230, 48, 123];    
    
let cond = arr.some(function(value:number,index:number) {    
console.log(value, index);  
return value < 10;  

});     
console.log(cond);        


  
  
                            **La méthode "every"**    
  _Cette méthode permet de tester un tableau et de vérifier toutes les valeurs du tableau valide la condition_    


let arr = [3, 4, 49 , 230, 48, 123];     
  
let cond = arr.every(function(value: number, index: number) {    
console.log(value, index);  
return value > ;  
});   
console.log('Toutes les valeurs:', cond);
  


