**Les classes**         
        
_Déclarer une classe_.      
        
Une classe est un type de fonction qui permet de crée des objets.       
    
_pour déclarer une classe on utilise le mots clef: class et le nom de class     
exemple:        
    
classe User {   

}       
let user = new User (); <<) **new user est l'instance  (objet)**    
let user2 = nex User();         
user.name = "Jean";  propriété 
    

console.log(user, user2);





**Les classes : Propriété par défaut**          
    
class User {    
  name = "Jean";    
  age = 22;     
presence;       
}       
let user = new User ();  ( l'instance est un Objet )  
let user2 = new User();     
    
user.name = "Sam";  
console.log(user, user2);   
        
        


            **Les classes : La fonction "constructor()"**   
_la fonction constructor permet de construire un nouvel objet._         
Exemple:    


class User {    
name = "Jean";    
age = 22;     
presence = false;        
pays = "France"
    
constructor(name, age, presence : boolean = false) }   
  this.name = name;     
  this.age = age;   
  this.presence = presence;
    
}    

}       
let user = new User (name:"Tom", age:27, presence true);  
let user2 = new User(name:"Sam", age:34);

user.name = "Sam";  
console.log(user, user2);       



****Les classes : Ajouter des méthodes****      

class User {    
name = "Jean";    
age = 22;     
presence = false;        
pays = "France"     

constructor(name, age, presence : boolean = false) }   
this.name = name;     
this.age = age;   
this.presence = presence;

}    
_**isPresent() {       
return this.presence;       
}**_             

_autre méthode   
say(str) {      
console.log(this.name + ' : ' + str);_


let user = new User (name:"Tom", age:27, presence true);  
let user2 = new User(name:"Sam", age:34);       
    
_user.say(str: "Bonjour" + user2.name);      
user2.say(str: "Bonjour" + user.name);_         


**Les classes : Héritage entre les classes**        

         
class Vehicule {    

   constructor(name, wheels);   
   this.name = name;    
   this.wheels = wheels;    
}       
        
accelerate()        
brake()     
turn()      
    
}
  



**Les classes : Propriétés et méthodes "static"**       
_les propriétés et les méthodes static sont des propriété et des méthodes qui ne sont pas accécible au niveaux des instances.       
    
quand on déclare une static se sont des constantes. exemple GRAVITY.        
exemple:        
        

