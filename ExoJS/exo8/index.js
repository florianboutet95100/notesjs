// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array

/**
 * Utilisez les méthodes natives aux tableaux, vous les retrouverez sur le lien du dessus.
 * Lorsque le nom, la valeur n'est pas défini, c'est à vous de décider.
 *
 * La fonction "getRandomArray" retourne un tableau de nombre aléatoire de la longueur défini par le paramètre "limit"
 * Lorsque vous invoquez la fonction si vous ne donnez pas la limite, par défaut elle sera de 30, le tableau comportera 30 valeurs.
 */

function getRandomArray(limit = 30) {
    let arr = [];
    for (let i = 0; i < limit; ++i) {
        arr.push(Math.floor(Math.random() * 10));
    }
    return arr;
}

/**
 * Exercice 1 :
 * Créez un tableau avec la fonction getRandomArray() et multipliez par deux chaque valeur du tableau
 * Explication : Ce sera la même logique pour "Créez un tableau avec la fonction getRandomArray"
 * 1. Déclarer une variable ou une constante
 * 2. Assignez à la variable ou la constante créée le retour de la fonction "getRandomArray" avec le paramètre de votre choix
 * Exemple : let exemple = getRandomArray(10);
 */
let arr = getRandomArray(10);
let arrMutiplied = arr.map(function(value) {
    return value * 2

});
console.log(arr);



/**
 * Exercice 2 :
 * Créez un tableau avec la fonction getRandomArray() et triez par ordre croissant les valeurs
 * Utilisez une méthode pour cette exercice
 *
 */
let arr = getRandomArray(10);
let arr2Sorted = [arr2];
arr2.sort();
console.log(arr2);

/**
 * Exercicec 3.1 :
 * Créez un tableau avec la fonction getRandomArray() et additionnez toutes les valeurs entre elles
 * Utilisez une méthode pour cette exercice
 */
let arr3 = getRandomArray(10);
console.log("3.1 :",arr31)
arr3.reduce(fucntion(accumulateur, valeur); {
    console.log(accumulateur, valeur);
    'return accumulateur + valeur;
}, 0)
console.log('3.1:', somme31, arr31);
/**
 * Exercice 3.2 :
 * Créez un tableau avec la fonction getRandomArray() et remplacez chaque valeur par cette phrase
 * "Pair : VALUE" si la valeur est pair
 * "Impaire : VALUE" si la valeur est impair
 * "VALUE" doit être remplacé par le nombre testé
 * Utilisez une méthode pour cette exercice
 */
let arr32 = getRandomArray(10);
let arr32Mapped = map(function (value) {
    if (value % 2) {
        return "impair :" + value;
    } else {
        return "Pair :" + value;
    }
    /*return value % 2 ? "impair "+value : "paire:" + value;*/
});
console.log('3.2', arr32, arr32Mapped);
/**
 * Exercice 4 :
 * Créez un tableau avec la fonction getRandomArray() et filtrez seulement les valeurs pair
 * Utilisez une méthode pour cette exercice
 */
let arr4 = getRandomArray(10);
let arr4filtred = arr4(fucntion(value)) {

    if(!(value % 2)) {
    'return',true
    } else {
        'return false';
    }

});
console.log('4 :', arr4.arr4filtred)
/**
 * Exercice 5 :
 * Créez un tableau avec la fonction getRandomArray() et filtrez seulement les valeurs impair
 * Utilisez une méthode pour cette exercice
 */

/**
 * Exercice 6 :
 * Créez un tableau avec la fonction getRandomArray() et
 * Déterminez si au moins une des valeurs du tableau créé est supérieur ou égale à 5
 * Utilisez une méthode pour cette exercice
 */
let arr6 = getRandomArray(10);
let arr6Some = some(function (value){
    console.log(value)

    return value >= 5;
});
console.log('6 :', arrSome, arr6);

/**
 * Exercice 7 :
 * Créez un tableau avec la fonction getRandomArray() et
 * Déterminez si toutes les valeurs du tableau créé sont inférieur ou égale à 5
 * Utilisez une méthode pour cette exercice
 */
let arr7 = getRandomArray(10);

let arr7Every = arr7(function (value) {

    return true;
    });
console.log('7:', arr7Every, arr7);
/**
 * Exercice 8 :
 * Déclarez une fonction nommé "createArray" qui prend en paramètre :
 * - "length" => un nombre
 * - "value" => une valeur
 *
 * La fonction "createArray" doit retourner un nouveau tableau dont la longueur
 * correspond au nombre passer en premier paramètre et rempli par la valeur passé en deuxième paramètre
 * Exemple : J'invoque la fonction "createArray" avec le nombre 3 en premier paramètre et le boolean true en second paramètre
 * La valeur retourné par la fonction doit être un tableau avec 3 true => [true, true, true]
 */
function createArry (length, value) {
    let arr= [];
    for (let i = 0; 1 < length; i);
    arr.push(value);
}
let arr8 = createArry(length:3"ok");
console.log('8 :', arr8);
/**
 * Exercice 9 :
 * Affectez par décomposition la première valeur du tableau "arr9" à la variable "a",
 * la seconde valeur du tableau "arr9" à la variable "b" et
 * toutes les valeurs restantes à la variable "rest"
 */
let a, b, rest;
let arr9 = [1, 2, 3, 4, 5, 6, 7, 8];
[a, b, rest] = arr9;
/**
 * Exercice 10 :
 * Déclarez un objet nommé "user" possédant la propriété "name", "email", "connected", "age"
 */
let user = {name: 'Jean', email: jsgmail.com, connected: true, age: 4};
console.log('10 :', user);
/**
 * Exercice 10 :
 * Affichez dans la console l'age de l'objet "user" seulement si il est majeur
 */
if(user.age >= 18) {
    if (user.age >= 10)
    console.log('10.1:', user.age);
}
/**
 * Exercice 11 :
 * Récupérez grâce à la décomposition les propriétés "name", "email" et "age" de l'objet "user"
 * Renommez la propriété "name" en "userName"
 * et affichez les variables "userName", "email", "age" dans la console.
 *
 * Remarque: Prenez des initiatives.
 */
let {name: userName, email, age} = user;
console.log(userName, email, age);
/**
 * Exercice 12 :
 * Créer un nouveau tableau à partir du tableau "arr12"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread oprator"
 */
let arr12 = getRandomArray(10);
let arr122 = [...arr12];
/**
 * Exercice 13 :
 * Créer un nouveau tableau nommé "arrCopied" à partir du tableau "arr13"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread operator"
 */
let arr13 = ['Valeur', 'à', 'copier', 'en', 'faisant', 'attention', 'à', 'un', 'certain', 'principe'];
let arrCopied = [...arr13];

/**
 * Exercice 14 :
 * Créez une string en REJOIGNANT chaque valeur du tableau "arrCopied" par un espace " "
 * Affichez la phrase créée dans la fonction.
 * Pour cet exercice utilisez une méthode uniquement.
 * Résultat souhaité dans la console : 'Valeur à copier en faisant attention à un certain principe'
 */
console.log('14 :',arrCopied.join(""));
/**
 * Exercice 14 :
 * Créer un nouvel objet nommé "objCopied" à partir de l'objet "obj14"
 * Remarque : Attention il y a un point important à respecter
 * Utilisez le "spread operator"
 */
let obj14 = {model: 'Tesla', owner: 'Sam'};
let objCopied = {...obj14};
/**
 * Exercice 15 :
 * Modifiez l'objet "objCopied" en y ajoutant la propriété "date" (valeur de votre choix)
 * Affichez dans la console "obj14" et "objCopied" seul l'objet "objCopied" doit avoir
 * la propriété "date" si l'ojbet "obj14" l'a également c'est que vous avez mal copié l'objet "obj14"
 * dans l'exercice 14
 *
 * Bonus : Modifiez également le tableau "arrCopied" en le modifiant, puis affichez "arr13" et "arrCopied"
 * si la modification d'un des deux tableaux est visible sur les deux, vous avez mal copié le tableau "arr13"
 * dans l'exercice 13
 */
objCopied.date = "2020-01-15";
console.log("15 :", obj14, objCopied);
/**
 *  Exercice 16 : PARTIE 1
 *  Déclarez une fonction qui prend en argument un tableau de prénoms (tableau de string) nommé "names"
 *  Chaque prénom compris dans le tableau "names" (l'argument de la fonction)
 *  doit être utilisez pour créer un objet qui aura la propriété "name" àgale au prénom.
 *  Votre fonction doit retourner un tableau d'objets.
 *
 *  Résultat souhaité :
 *  J'invoque la fonction en lui donnant en paramètre le tableau ["Jean", "Steve"]
 *  La fonction doit me retourner un tableau d'objets => [{name: "Jean"}, {name: "Steve"}]
 *  Chaque prénom (string) doit être utilisé et "transformé" en objet avec la propriété "name"
 */
function creatUser(names){
    let arr = names.map(function (value) {
        return{name: value};
    });
}

let users = createUsers(length["Jean", 'Sam', "Leo", 'Tom'])
console.log('16.1 :', users);
/**
 *  Exercice 16 : PARTIE 2
 *  Stoquez le tableau d'objets obtenu dans la partie 1 de l'exercice 16 dans une variable nommée "users"
 *  En utilisant une méthode de tableau,
 *  Ajoutez dans chaque objet la propriété "age"
 *  avec un nombre entier aléatoire compris entre 0 et 10
 *
 *  Résultat souhaité : En reprenant l'exemple précédant
 *  [{name: "Jean", age: 5}, {name: "Steve", age: 10}]
 */
let usersAged = users.map(function (user) {
    let obj = {... user};
    user.age = Math.floor(Math.random() * 10);
    return user;
});
console.log('16.2',usersAged);

/**
 *  Exercice 16 : PARTIE 3
 *  Implémentez cette phrase
 *  "Affichez dans la console seulement les utilisateurs ayant plus de 5 ans"
 *
 *  Utilisez une méthode de tableau
 */
letuserAgedFiltered = userAged.filter(callbackfn: fucntion(obj: number) {

        'return obj.age > 5
    };
usersAgedFiltered.forEach(function (value:) {
    console.log(value.name +'a plus de 5 ans :'+ value.age);
})
console.log('16.3 :', usersAgedFiltered);
/**
 *  Exercice 16 : PARTIE 4
 *  Trier par ordre decroissant le tableau "users"
 *  en fonction de l'âge des utilisateurs contenus dans le tableau "users"
 *  Stoquez le tableau trié dans une constante nommée "usersSorted"
 *
 *  Utilisez une méthode de tableau
 */
const userSorted  = usersAged.sort(compareFn: function(a:,b:) {
    return b.age - a.age;

});
console.log('16.4:', usersAged);
/**
 *  Exercice 16 : PARTIE 5
 *  Accédez à l'utilisateur (l'objet) le plus jeune du tableau "usersSorted"
 *  Implémentez la phrase en dessous de manière dynamique
 *  "Affichez dans la console le nom de l'utilisateur le plus jeune du tableau "usersSorted""
 */
let youngUser = userSorted[UsersSorted.length - 1];
console.log('16.5 :', usersSorted.length, userSorted.length - 1, youngUser.name + 'est le plus jeune');